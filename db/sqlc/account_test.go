package db

import (
	"context"
	"github.com/stretchr/testify/require"
	"gitlab.com/kurniawanmasriza/go-simplebank/util"
	"testing"
)

func createRandomAccount(arg CreateAccountParams) (Account, error) {
	account, err := testQueries.CreateAccount(context.Background(), arg)

	return account, err
}

func TestCreateAccount(t *testing.T) {
	arg := CreateAccountParams{
		Owner:    util.RandomString(10),
		Balance:  util.RandomInt(1, 1000),
		Currency: util.RandomCurrency(),
	}

	account, err := createRandomAccount(arg)

	require.NoError(t, err)
	require.NotEmpty(t, account)
	require.NotZero(t, account.ID)
	require.Equal(t, arg.Owner, account.Owner)
}
